# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:filetype=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem 1.0

name            GiNaC
version         1.6.6
categories      math
platforms       darwin
license         GPL-2
maintainers     gmail.com:mschamschula openmaintainer
description     GiNaC is Not a Computer algebra system
long_description \
    Currently, GiNaC's capabilities include: \
        1. Very fast manipulation of large integers and \
           rationals owing to the CLN library (for instance, \
           it uses Karatsuba multiplication and \
           Schoenhage-Strassen multiplication for very large \
           integers). \
        2. Efficient handling of multivariate polynomials and \
           rational functions. \
        3. Support for linear algebra includes symbolic \
           matrices, vectors and solving equations. \
        4. Very fast heuristic polynomial GCD. \
        5. Many built in functions (sin, cos, atan, sinh, \
           factorial, etc.) \
        6. Symbolic differentiation and series expansion of \
           all built-in functions. \
        7. Several forms of output (also as optimized C++, \
           for numerical postprocessing). \
        8. Memory-efficiency and -safety through the internal \
           use of reference counting for all expressions.

use_bzip2       yes
distname        ginac-${version}

homepage        http://www.ginac.de/
master_sites    ${homepage}

checksums       rmd160  387aae769b9e55a8abe362f02ee26225995d97d0 \
                sha256  25ec6d535ee77caf6161843688489cfc319b6c4fda46c5d7878587ee5562ddce

depends_build   port:pkgconfig
depends_lib     port:readline \
                port:cln

#patches for clang++/libc++; do not seem to hurt on g++/libstdc++

patchfiles      patch-check_exam_clifford.cpp.diff

test.run        yes
test.target     check

livecheck.type      regex
livecheck.url       ${homepage}News.html
livecheck.regex     "Version (\[0-9.\]+)"
