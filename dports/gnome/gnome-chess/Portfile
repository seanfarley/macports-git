# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:filetype=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0

name                gnome-chess
version             3.18.0
license             GPL-2+
set branch          [join [lrange [split ${version} .] 0 1] .]
description         Play the classic two-player boardgame of chess
long_description    ${description}
maintainers         devans openmaintainer
categories          gnome games
platforms           darwin
homepage            https://wiki.gnome.org/Apps/Chess
master_sites        gnome:sources/${name}/${branch}/

use_xz              yes

checksums           rmd160  2b30efcf1bcec6eb97d0bf90203b5470688df86c \
                    sha256  c841198935d807618c3cecbf10ed24643390d504e17961717bac455f4e1b37ad

depends_build       port:pkgconfig \
                    port:appstream-glib \
                    port:yelp-tools \
                    port:itstool \
                    port:intltool \
                    port:autoconf \
                    port:automake \
                    port:libtool

depends_lib         port:desktop-file-utils \
                    port:gtk3 \
                    port:librsvg \
                    port:vala

depends_run         port:gnome-themes-standard \
                    port:yelp

# reconfigure using upstream autogen.sh for intltool 0.51 compatibility

post-patch {
    xinstall -m 755 ${filespath}/autogen.sh ${worksrcpath}
}

configure.cmd       ./autogen.sh

configure.args      --disable-schemas-compile \
                    --disable-silent-rules

post-activate {
   system "${prefix}/bin/gtk-update-icon-cache-3.0 -f -t ${prefix}/share/icons/hicolor"
   system "${prefix}/bin/update-desktop-database ${prefix}/share/applications"
   system "${prefix}/bin/glib-compile-schemas ${prefix}/share/glib-2.0/schemas"
}

notes "
To play against the computer, install a chess engine such as gnuchess.
See https://wiki.gnome.org/Apps/Chess/ChessEngines
"

livecheck.type      gnome
