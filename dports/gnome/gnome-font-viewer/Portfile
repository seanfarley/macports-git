# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:filetype=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0

name                gnome-font-viewer
version             3.16.2
revision            2
license             GPL-2
set branch          [join [lrange [split ${version} .] 0 1] .]
description         GNOME font viewer and thumbnailer.
long_description    GNOME font viewer can preview fonts and create thumbnails for fonts.

maintainers         juanrgar devans openmaintainer
categories          gnome
platforms           darwin
homepage            https://www.gnome.org
master_sites        gnome:sources/${name}/${branch}/

use_xz              yes

checksums           rmd160  10c7e30dc83341657adb496dbf48b1a55120fc50 \
                    sha256  f5367417d926d1dbe175aceb8eb4d2733d723a237428f130edd4b4023c0dc1cf

depends_build       port:pkgconfig \
                    port:intltool \
                    port:gnome-common \
                    port:autoconf \
                    port:automake \
                    port:libtool

depends_lib         port:desktop-file-utils \
                    port:gsettings-desktop-schemas \
                    port:gtk3 \
                    port:gnome-desktop \
                    port:fontconfig \
                    port:freetype

depends_run         port:gnome-themes-standard

# reconfigure using upstream autogen.sh for intltool 0.51 compatibility

post-patch {
    xinstall -m 755 ${filespath}/autogen.sh ${worksrcpath}
}

configure.cmd       ./autogen.sh

configure.args      --disable-schemas-compile \
                    --disable-silent-rules

# if port gnome-utils is installed
# and gnome-font-viewer binary exists
# and port gnome-font-viewer is NOT installed
# deactivate outdated port gnome-utils
 
pre-activate {
    if {![catch {registry_active gnome-utils}]} {
        if {[file exists ${prefix}/bin/gnome-font-viewer]} {
            if {[catch {registry_active gnome-font-viewer}]} {
                registry_deactivate_composite gnome-utils "" [list ports_nodepcheck 1]
            }
        }
    }
}

# port installs desktop application file
post-activate {
    system "${prefix}/bin/update-desktop-database ${prefix}/share/applications"
}

livecheck.type      gnome
