# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0

name                seahorse
version             3.18.0
revision            1
license             GPL-2+ GFDL-1.1+
set branch          [join [lrange [split ${version} .] 0 1] .]
description         Seahorse is a graphical interface for managing and using encryption keys.
long_description    ${description} \
                    Currently it supports PGP keys (using GPG/GPGME) and SSH keys. Its goal is to \
                    provide an easy to use Key Management Tool, along with an easy to use interface \
                    for encryption operations.
maintainers         devans openmaintainer
categories          gnome crypto
platforms           darwin
homepage            https://wiki.gnome.org/Apps/Seahorse
master_sites        gnome:sources/${name}/${branch}/

use_xz              yes

checksums           rmd160  50316d4704f2dc2ed16b82a5d120ce67c4d08927 \
                    sha256  530c889a01c4cad25df4c9ab58ab95d24747875789bc6116bef529d60fc1b667

depends_build       port:pkgconfig \
                    port:intltool \
                    port:itstool \
                    port:yelp-tools \
                    port:gnome-common \
                    port:autoconf \
                    port:automake \
                    port:libtool

depends_lib         port:desktop-file-utils \
                    port:gtk3 \
                    port:gcr \
                    port:gpgme \
                    port:openldap \
                    port:libsoup \
                    port:libsecret \
                    port:openssh \
                    port:vala

depends_run         port:gnome-themes-standard \
                    port:yelp

# reconfigure using upstream autogen.sh for intltool 0.51 compatibility

post-patch {
    xinstall -m 755 ${filespath}/autogen.sh ${worksrcpath}
}

configure.cmd       ./autogen.sh

configure.cflags-append \
                    -Wno-return-type

configure.args      --disable-sharing \
                    --disable-schemas-compile \
                    --disable-silent-rules

post-activate {
    system "${prefix}/bin/update-desktop-database -q ${prefix}/share/applications"
    system "${prefix}/bin/gtk-update-icon-cache-3.0 -f -t ${prefix}/share/icons/hicolor"
    system "${prefix}/bin/glib-compile-schemas ${prefix}/share/glib-2.0/schemas"
}

livecheck.type      gnome
