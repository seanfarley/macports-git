# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:filetype=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           gobject_introspection 1.0

name                geocode-glib
version             3.18.1
license             LGPL-2.1
set branch          [join [lrange [split ${version} .] 0 1] .]
description         A convenience library for geocoding and reverse geocoding.
long_description    geocode-glib is a convenience library for geocoding (finding longitude, \
                    and latitude from an address) and reverse geocoding (finding an address from \
                    coordinates). It uses the Nominatim service to achieve that. It also caches \
                    (reverse-)geocoding requests for faster results and to avoid unnecessary \
                    server load.

maintainers         devans openmaintainer
categories          gnome devel
platforms           darwin
homepage            https://developer.gnome.org/geocode-glib/
master_sites        gnome:sources/${name}/${branch}/

use_xz              yes

checksums           rmd160  2edfc5a8cbac0817e1222a8023e666cd99351c92 \
                    sha256  75d12bf82575449b8290b7463e8b6cf1b99f2c9942db6391a3d5b0bbb600c365

depends_build       port:pkgconfig \
                    port:gettext

depends_lib         path:lib/pkgconfig/glib-2.0.pc:glib2 \
                    port:json-glib \
                    port:libxml2 \
                    port:libsoup

gobject_introspection yes

configure.args      --enable-compile-warnings=minimum \
                    --disable-silent-rules

livecheck.type      gnome
