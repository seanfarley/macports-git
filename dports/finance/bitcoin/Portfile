# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0

name                bitcoin
categories          finance crypto
version             0.11.2
platforms           darwin
license             MIT
maintainers         easieste yopmail.com:sami.laine openmaintainer
description         client user interface for a peer-to-peer digital currency
long_description    Bitcoin is a peer-to-peer digital currency. By peer-to-peer,  \
                    we mean that there is no central authority to issue \
                    new Bitcoins or keep track of transactions. Instead, these \
                    tasks are managed collectively by the Bitcoin protocol \
                    operating through the nodes of the network.

homepage            https://bitcoin.org/
master_sites        ${homepage}bin/bitcoin-core-${version}/

checksums           rmd160  df541d9ac63bbea19f8d021326293270cea50dfa \
                    sha256  a4d2bd642e5f7f1f82dc3f708618ac77e1e45353db7a98bf81c3bdc0e10690d3

depends_build       port:pkgconfig \
                    port:autoconf \
                    port:automake \
                    port:libtool

depends_lib         port:boost \
                    path:lib/libssl.dylib:openssl \
                    port:db48 \
                    port:miniupnpc \
                    port:protobuf-cpp

use_parallel_build  no

#patchfiles      \
#    src-rpcrawtransaction.cpp.diff # for 0.10.1

configure.args      --disable-ccache \
                    --disable-silent-rules

# TODO: Restore GUI: needs to move to qt5, as it doesn't look like qt4 is going to get fixed.
default_variants     +daemon

variant gui description {NOT WORKING Build with qt4 (Broken in qt4 moc inclusion of boost headers) } {
    configure.cmd-append    --with-gui=qt4
    depends_lib-append      port:qt4-mac
    build.args-add          appbundle
}

variant daemon description {Build and install only the bitcoind daemon} {
    configure.cmd-append    --with-gui=no
    build.args-delete       appbundle
}

destroot {
#    if {![variant_isset daemon]} {
#        copy ${worksrcpath}/Bitcoin-Qt.app ${destroot}${applications_dir}
#    }
    xinstall -W ${worksrcpath}/src bitcoin-cli bitcoind ${destroot}${prefix}/bin

    set docdir ${prefix}/share/doc/${name}
    xinstall -d ${destroot}${docdir}
    xinstall -m 444 -W ${worksrcpath} {*}[glob ${worksrcpath}/doc/*.md] ${destroot}${docdir}
}

# TODO
#
## Automate creation of nonce RPC credentials
## Add launchctl hooks for daemon

notes "

1.  Sanity in scripting:

    cmd$ ln -s \"~/Library/Application Support/Bitcoin\" ~/.bitcoin

2.  View progress with:

    cmd$ tail -F ~/.bitcoin/debug.log

3.  Edit ~/.bitcoin/bitcoin.conf with RPC credentials

4.  Start in background via:
    
    cmd$ ${prefix}/sbin/bitcoind -daemon

"

livecheck.type      regex
livecheck.url       ${homepage}bin/
livecheck.regex     ${name}-core-(\[0-9.\]+)/
