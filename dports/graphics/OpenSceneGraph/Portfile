# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem              1.0
PortGroup               cmake 1.0

name                    OpenSceneGraph
conflicts               OpenSceneGraph-devel
version                 3.4.0
platforms               darwin
categories              graphics
maintainers             nomaintainer
license                 wxWidgets-3
description             high-performance 3D graphics toolkit

long_description        ${name} is a high-performance 3D graphics toolkit useful in fields such as \
                        visual simulation, games, virtual reality, scientific visualization and \
                        modelling.

homepage                http://www.openscenegraph.org/
master_sites            ${homepage}downloads/stable_releases/${name}-${version}/source

use_zip                 yes

checksums               rmd160  6043321fc1c9b23f0e536c4e18a96a97d8b16c30 \
                        sha256  5c727d84755da276adf8c4a4a3a8ba9c9570fc4b4969f06f1d2e9f89b1e3040e

# Tons of stuff was disabled originally, but leaving it in seems to work:
#patchfiles              patch-CMakeLists.txt.diff

depends_build-append    port:pkgconfig

depends_lib             port:freetype \
                        port:jasper \
                        port:openexr \
                        port:zlib \
                        port:gdal \
                        port:curl \
                        path:lib/libavcodec.dylib:ffmpeg \
                        port:poppler \
                        port:librsvg \
                        port:giflib \
                        port:tiff \
                        port:boost

configure.args-append   -DOSG_CONFIG_HAS_BEEN_RUN_BEFORE=YES \
                        -DOSG_DEFAULT_IMAGE_PLUGIN_FOR_OSX=imageio \
                        -DOSG_WINDOWING_SYSTEM=Cocoa \
                        -DOSG_USE_QT:BOOL=OFF

variant qt5 description "with Qt5 support" {
	configure.args-delete -DOSG_USE_QT:BOOL=OFF
	configure.args-append -DOSG_USE_QT:BOOL=ON -DDESIRED_QT_VERSION=5
}

livecheck.type          regex
livecheck.url           ${homepage}downloads/stable_releases/
livecheck.regex         "${name}-(\\d+\\.\\d*\[02468\](?:\\.\\d+)*)"

