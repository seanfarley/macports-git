# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:filetype=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           perl5 1.0

name                imapsync
version             1.607
revision            1
categories          mail
platforms           darwin
maintainers         mww openmaintainer
license             WTFPL-2
description         imapsync is an IMAP synchronization, copy or migration tool.
long_description    imapsync is an IMAP synchronization, copy or migration \
                    tool. Synchronize mailboxes between two imap servers. It is \
                    especially good at IMAP migration.

homepage            http://www.linux-france.org/prj/imapsync/
master_sites        http://fedorahosted.org/released/imapsync/
extract.suffix      .tgz

checksums           rmd160  a22b1577278097cb1a79edf3500eab983ab86601 \
                    sha256  784331cfa6cc391751dcdc5290eba5d50bf3ddbe9b213f072b413941a3fe4f2a

perl5.branches      5.22
depends_run         port:perl${perl5.major} \
                    port:p${perl5.major}-digest-md5 \
                    port:p${perl5.major}-mail-imapclient \
                    port:p${perl5.major}-term-readkey \
                    port:p${perl5.major}-io-socket-ssl \
                    port:p${perl5.major}-io-tee \
                    port:p${perl5.major}-datemanip \
                    port:p${perl5.major}-digest-hmac \
                    port:p${perl5.major}-file-copy-recursive

supported_archs     noarch

post-patch {
    reinplace -locale C "s|^#!.*|#!${perl5.bin}|g" ${worksrcpath}/imapsync
}

use_configure       no

build               {}

destroot {
    xinstall -m 755 ${worksrcpath}/imapsync ${destroot}${prefix}/bin/imapsync.pl
    ln -sf imapsync.pl ${destroot}${prefix}/bin/imapsync
    xinstall -m 755 -d ${destroot}${prefix}/share/doc/${name}
    xinstall -m 644 -W ${worksrcpath} CREDITS ChangeLog FAQ LICENSE INSTALL README \
        TODO VERSION ${destroot}${prefix}/share/doc/${name}
}

livecheck.type      regex
livecheck.url       [lindex ${master_sites} 0]
livecheck.regex     ${name}-(\[.0-9\]+)\\.tgz
