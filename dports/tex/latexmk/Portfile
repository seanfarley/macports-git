# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0

name                latexmk
version             4.43a
checksums           rmd160  e7e3703a52bf070a780cbb6d50f6d79e3aa9bc3e \
                    sha256  e410d295c0a47327b953ece5b582c294359bdf89138ef990d5621b020ff2bbe5

categories          tex print
platforms           darwin
maintainers         cal openmaintainer
supported_archs     noarch
license             GPL-2+

description         Automates the process of generating a LaTeX document
long_description \
    Latexmk completely automates the process of generating a LaTeX document. \
    Essentially, it is a highly specialized cousin of the general make       \
    utility.  Given the source files for a document, latexmk issues the      \
    appropriate sequence of commands to generate a .dvi, .ps, .pdf or        \
    hardcopy version of the document.  It can also be set to run             \
    continuously with a previewer\; the latex program, etc, are rerun        \
    whenever one of the source files is modified.

homepage            http://www.phys.psu.edu/~collins/software/latexmk-jcc/
master_sites        ${homepage}
distname            ${name}-[string map {. ""} $version]

use_zip             yes
worksrcdir          ${name}

depends_run         bin:latex:texlive-latex

use_configure       no
build               {}

destroot {
    set bindir ${destroot}${prefix}/bin
    file mkdir ${bindir}
    xinstall -m 755 -v ${worksrcpath}/${name}.pl ${bindir}/${name}
    set mandir ${destroot}${prefix}/share/man/man1
    file mkdir ${mandir}
    xinstall -m 644 -v ${worksrcpath}/${name}.1 ${mandir}
    set docdir ${destroot}${prefix}/share/doc/${name}
    file mkdir ${docdir}
    xinstall -m 644 -v -W ${worksrcpath} \
        CHANGES COPYING README ${name}.pdf ${name}.txt ${docdir}
}

livecheck.type      regex
livecheck.regex     "(\[0-9a-z.\]+) dated"
