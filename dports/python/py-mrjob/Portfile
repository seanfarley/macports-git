# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           python 1.0
PortGroup           github 1.0

github.setup        Yelp mrjob 0.4.2 v

name                py-${github.project}
categories-append   parallel
platforms           darwin
supported_archs     noarch
license             Apache-2
maintainers         petr openmaintainer

description         Python MapReduce framework
long_description    \
    mrjob is a Python package that helps you write and run Hadoop Streaming \
    jobs. It fully supports Amazon's Elastic MapReduce (EMR) service, which \
    allows you to buy time on a Hadoop cluster on an hourly basis. It also \
    works with your own Hadoop cluster.

homepage            https://pythonhosted.org/mrjob/

checksums           md5     6402f7ab4755722fa37fd814411a9873 \
                    rmd160  e63e13b9449633e31418c60a0caa2f69100032d6 \
                    sha256  939daf60617b46b99899fb51ded4b690bf107675f4c889bca92e4a4dd689ad41

python.versions     27

if {${name} ne ${subport}} {
    depends_build-append    port:py${python.version}-setuptools

    depends_lib-append    port:py${python.version}-yaml \
                          port:py${python.version}-boto

    # Adding documentation and examples
    post-destroot {
        set dest_doc ${destroot}${prefix}/share/doc/${subport}
        xinstall -d  ${dest_doc}
        xinstall -m 755 -W ${worksrcpath} \
            README.rst \
            LICENSE.txt \
            CHANGES.txt \
            CONTRIBUTING.rst \
                ${dest_doc}
    }

    livecheck.type  none
}
