# -*- coding: utf-8; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           python 1.0

name                py-scikits-bootstrap
version             0.3.2
categories-append   science
license             BSD
platforms           darwin
maintainers         aronnax
description         Bootstrap confidence interval estimation routines for SciPy.
long_description    Algorithms for SciPy to calculate bootstrap confidence \
                    intervals for statistics functions applied to data.
homepage            http://github.org/cgevans/scikits-bootstrap
master_sites        http://pypi.python.org/packages/source/s/scikits.bootstrap/

distname            scikits.bootstrap-${version}
worksrcdir          scikits-bootstrap-${version}

checksums           md5     c4e55db940f078759b52b32cdf394224 \
                    rmd160  f13d0cde10cfbea234421ffd3f13e849f4854a3d \
                    sha256  0f3e335060de3b768c9eb1a4938097323808b75724e2bcdec609dc5143bcfa3a

python.versions     26 27 33 34

if {${name} ne ${subport}} {
    depends_lib-append  port:py${python.version}-setuptools \
                        port:py${python.version}-numpy \
                        port:py${python.version}-scipy

    livecheck.type      none
} else {
    livecheck.type      regex
    livecheck.url       ${master_sites}
    livecheck.regex     "scikits\\.bootstrap-(\\d+(?:\\.\\d+)*)${extract.suffix}"
}
