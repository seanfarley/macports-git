# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           python 1.0

name                py-bcolz
version             0.12.1
revision            0
categories-append   devel
platforms           darwin
license             BSD

python.versions     27 34 35

maintainers         stromnov openmaintainer

description         Columnar and compressed data containers.

long_description    bcolz provides columnar and compressed data containers. \
                    Column storage allows for efficiently querying tables \
                    with a large number of columns. It also allows for cheap \
                    addition and removal of column. In addition, bcolz \
                    objects are compressed by default for reducing \
                    memory/disk I/O needs. The compression process is \
                    carried out internally by Blosc, a high-performance \
                    compressor that is optimized for binary data.

homepage            https://github.com/Blosc/bcolz
master_sites        pypi:[string index ${python.rootname} 0]/${python.rootname}

distname            ${python.rootname}-${version}

checksums           rmd160  b0171e77c19e78eb9adf5a98ee3e7ed49da90d87 \
                    sha256  a8dafa42cd4f3ca130ecb81f7e778204a12c2180c18fd570ef753de58ee7ddbd

if {${name} ne ${subport}} {
    # Fix permissions
    post-extract {
        fs-traverse item ${worksrcpath} {
            if {[file isdirectory ${item}]} {
                file attributes ${item} -permissions a+rx
            } elseif {[file isfile ${item}]} {
                file attributes ${item} -permissions a+r
            }
        }
    }

    depends_build-append \
                        port:py${python.version}-cython

    depends_lib-append  port:py${python.version}-numpy \
                        port:blosc

    build.args-append   --blosc=${prefix}

    livecheck.type      none
} else {
    livecheck.type      pypi
}
