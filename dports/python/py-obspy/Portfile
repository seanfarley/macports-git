# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           python 1.0

set _name           obspy
set _n              [string index ${_name} 0]

name                py-obspy
conflicts           ${name}-devel

version             0.10.2
revision            2
categories-append   science
platforms           darwin
license             LGPL-3

maintainers         petr openmaintainer

description         Python framework for processing seismological data

long_description    \
    ObsPy is an open-source project dedicated to provide a Python framework \
    for processing seismological data. It provides support for file formats \
    and signal processing routines which allow the manipulation, analysis \
    and visualization of seismological time series. The goal of the ObsPy \
    project is to facilitate rapid application development for seismology.

homepage            http://www.obspy.org/

use_zip             yes
distname            ${_name}-${version}
master_sites        https://pypi.python.org/packages/source/${_n}/${_name}/

checksums           md5     7a77c6b96143e6758c38fed0775d2b16 \
                    rmd160  17d7cfd97472fc1fce34fe36b90f98da6c3d2c0b \
                    sha256  9af1b349204a1447a1ceac9b1844295297bb40217c98d8e01b62d51e6ceda609

python.versions     27 34

if {${name} ne ${subport}} {
    depends_build-append    port:py${python.version}-numpy

    depends_lib-append      port:py${python.version}-future \
                            port:py${python.version}-lxml \
                            port:py${python.version}-matplotlib \
                            port:py${python.version}-numpy \
                            port:py${python.version}-scipy \
                            port:py${python.version}-setuptools \
                            port:py${python.version}-sqlalchemy

    # py-numpy & py-scipy are not universal
    universal_variant       no

    # fix wrong file permissions of the distfile
    post-extract {
        fs-traverse f ${worksrcpath} {
            file attributes ${f} -permissions a+r  
        }
    }

    livecheck.type  none
} else {
    livecheck.type  regex
    livecheck.url   https://pypi.python.org/pypi/${_name}/json
    livecheck.regex "\"${_name}-(\[.\\d\]+)\\${extract.suffix}\""
}
