# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem      1.0
PortGroup           python 1.0

name                py-shapely
version             1.5.13
python.versions     27 34 35
categories-append     devel
platforms       darwin
maintainers     snc openmaintainer
license             BSD
homepage            http://toblerity.org/shapely/
description     Shapely is a Python package for manipulation \
                and analysis of 2D geospatial geometries

long_description \
                Shapely is a Python package for manipulation and \
                analysis of 2D geospatial geometries. It is based on \
                GEOS (http://geos.refractions.net). Shapely 1.0 is not \
                concerned with data formats or coordinate reference \
                systems.

distname            Shapely-${version}
master_sites        https://pypi.python.org/packages/source/S/Shapely

checksums           rmd160  ec43a5e5e84bc6df9f7b630fc3b6c14c6bb4c55b \
                    sha256  68f8efb43112e8ef1f7e56e2c9eef64e0cbc1c19528c627696fb07345075a348

if {${name} ne ${subport}} {
    depends_lib-append      port:geos
    depends_build-append    port:py${python.version}-cython \
                            port:py${python.version}-setuptools

    patchfiles      patch-shapely_geos.py.diff

    post-patch {
            reinplace "s|@PREFIX@|${prefix}|g" ${worksrcpath}/shapely/geos.py
            reinplace s|\\\"cython\\\"|"${python.prefix}/bin/cython"| ${worksrcpath}/setup.py
    }

    livecheck.type      none
} else {
    livecheck.type          regex
    livecheck.url           https://pypi.python.org/pypi/Shapely/json
    livecheck.regex         {Shapely-(\d+(?:\.\d+)*)\.[tz]}
}
