# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0

name                libctl
version             3.2.2
revision            1
categories          science
platforms           darwin
license				GPL-2+

maintainers         saabusa.com:Yogesh.Sharma openmaintainer

description         Scheme/Guile-based scripting of scientific code.
long_description    \
	Libctl is a free Guile-based library implementing flexible control files \
	for scientific simulations. It was written to support the MIT Photonic \
	Bands and Meep software, but has proven useful in other programs too.

homepage            http://ab-initio.mit.edu/wiki/index.php/Libctl
master_sites        http://ab-initio.mit.edu/libctl/

checksums           md5     5fd7634dc9ae8e7fa70a68473b9cbb68 \
                    sha1    d7f860313d5cc226c51f868bbe9bb930d143ab9c \
                    rmd160  2390548f7a30e709e22b3ee12f21ec9f7e45def7

depends_lib         port:guile

variant gcc45 conflicts gcc46 gcc47 gcc48 description {Compile with gcc 4.5} {
    configure.compiler	macports-gcc-4.5
}

variant gcc46 conflicts gcc45 gcc47 gcc48 description {Compile with gcc 4.6} {
    configure.compiler	macports-gcc-4.6
}

variant gcc47 conflicts gcc45 gcc46 gcc48 description {Compile with gcc 4.7} {
    configure.compiler	macports-gcc-4.7
}

variant gcc48 conflicts gcc45 gcc46 gcc47 description {Compile with gcc 4.8} {
    configure.compiler	macports-gcc-4.8
}

if {![variant_isset gcc45] && ![variant_isset gcc46] && ![variant_isset gcc47]} {
    default_variants +gcc48
}

use_autoreconf		yes
autoreconf.args		-fvi
