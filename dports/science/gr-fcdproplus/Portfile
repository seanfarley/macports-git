# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           cmake 1.0
PortGroup           github 1.0

github.setup        dl1ksv gr-fcdproplus cf5db388e9f2f77b9ad2ff727608d5cf89d8e218
version             20150330
revision            5
checksums           rmd160 5d91fef65fd6adaff99645448f249cbc676a2d83 \
                    sha256 cbc9310127af8d6d501f06f46eb47a7e377d74aaf45e95f70bb53dd55d0a13bd

categories          science comms
maintainers         michaelld openmaintainer
description         Implements a funcube dongle pro+ source in GNU Radio.
long_description    ${description}
license             GPL-3
platforms           darwin

depends_build-append port:pkgconfig
depends_lib-append	port:boost \
                    port:doxygen \
                    path:lib/libusb-1.0.dylib:libusb \
                    port:swig-python

# allow gr-fcdproplus to work with both gnuradio and gnuradio-devel ...

depends_lib-append  path:lib/libgnuradio-runtime.dylib:gnuradio

# ... but not with gnuradio-next

pre-fetch {
    if {![catch {set installed [lindex [registry_active gnuradio-next] 0]}]} {
        # gnuradio-next is installed; this version of gr-fcdproplus does not work with gnuradio-next
        ui_msg "\nError: ${name} requires the gnuradio or gnuradio-devel port, and will not work with the gnuradio-next port.  deactivate gnuradio-next, and then install or activate gnuradio or gnuradio-devel.\n"
        return -code error "Invalid port dependency: gnuradio-next"
    }
}

# do VPATH (out of source tree) build

cmake.out_of_source yes

# remove top-level library path, such that internal libraries are used
# instead of any already-installed ones.

configure.ldflags-delete -L${prefix}/lib

# specify the Python dependencies

depends_lib-append \
    port:python27

# specify the Python version to use

configure.args-append \
    -DPYTHON_EXECUTABLE=${prefix}/Library/Frameworks/Python.framework/Versions/2.7/bin/python2.7 \
    -DPYTHON_INCLUDE_DIR=${prefix}/Library/Frameworks/Python.framework/Versions/2.7/Headers \
    -DPYTHON_LIBRARY=${prefix}/Library/Frameworks/Python.framework/Versions/2.7/Python \
    -DGR_PYTHON_DIR=${frameworks_dir}/Python.framework/Versions/2.7/lib/python2.7/site-packages
