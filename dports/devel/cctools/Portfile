# $Id$

PortSystem              1.0

name                    cctools
# Xcode 7.0
version                 877.5
revision                2
set ld64_version        253.3
categories              devel
platforms               darwin
maintainers             jeremyhu openmaintainer
license                 {APSL-2 GPL-2+}
installs_libs           no
description             Compiler Tools for Mac OS X and Darwin
long_description        A set of essential tools to support development \
                        on Mac OS X and Darwin. Conceptually similar \
                        similar to binutils on other platforms.

homepage                http://opensource.apple.com/source/${name}/
master_sites            http://opensource.apple.com/tarballs/${name} \
                        http://opensource.apple.com/tarballs/ld64

distfiles               ld64-${ld64_version}.tar.gz ${name}-${version}.tar.gz

checksums               ld64-253.3.tar.gz \
                        rmd160  05a334fa636bf05a36eac3e745101f8f2dcab70b \
                        sha256  76c02f6f297c251b66504e1115946bda6e1618640bc6cf03d0ad99b17bd8a5d6 \
                        cctools-877.5.tar.gz \
                        rmd160  a4cc2423f491cd09316d4ff63919d850d05f09a5 \
                        sha256  5e7525c86581d9dead8a8508a9e824101765d05a3e14d612de1aa38def9adde9

depends_build           port:libunwind-headers

patchfiles \
    cctools-829-lto.patch \
    PR-37520.patch \
    cctools-839-static-dis_info.patch \
    PR-12400897.patch \
    PR-24031030.patch \
    cctools-862-prunetrie.patch \
    not-clang.patch \
    cctools-877-no-lto.patch

variant universal {}

if {${os.major} < 11} {
    patchfiles-append snowleopard-strnlen.patch
}

if {![variant_isset llvm33] && ![variant_isset llvm34] && ![variant_isset llvm35] && ![variant_isset llvm36] && ![variant_isset llvm37] && ![variant_isset llvm38]} {
    if {${os.major} >= 13} {
        default_variants +llvm37
    } elseif {${os.major} >= 10} {
        default_variants +llvm34
    } elseif {${os.major} >= 9} {
        default_variants +llvm33
    }
}

set llvm_version {}
variant llvm33 conflicts llvm34 llvm35 llvm36 llvm37 llvm38 description {Use llvm-3.3 for libLTO and llvm-mc} {
    set llvm_version        3.3
    depends_lib-append      port:llvm-${llvm_version}
}

variant llvm34 conflicts llvm33 llvm35 llvm36 llvm37 llvm38 description {Use llvm-3.4 for libLTO and llvm-mc} {
    set llvm_version        3.4
    depends_lib-append      port:llvm-${llvm_version}
}

variant llvm35 conflicts llvm33 llvm34 llvm36 llvm37 llvm38 description {Use llvm-3.5 for libLTO and llvm-mc} {
    set llvm_version        3.5
    depends_lib-append      port:llvm-${llvm_version}
}

variant llvm36 conflicts llvm33 llvm34 llvm35 llvm37 llvm38 description {Use llvm-3.6 for libLTO and llvm-mc} {
    set llvm_version        3.6
    depends_lib-append      port:llvm-${llvm_version}
}

variant llvm37 conflicts llvm33 llvm34 llvm35 llvm36 llvm38 description {Use llvm-3.7 for libLTO and llvm-mc} {
    set llvm_version        3.7
    depends_lib-append      port:llvm-${llvm_version}
}

variant llvm38 conflicts llvm33 llvm34 llvm35 llvm36 llvm37 description {Use llvm-3.8 for libLTO and llvm-mc} {
    set llvm_version        3.8
    depends_lib-append      port:llvm-${llvm_version}
}

use_configure           no
destroot.args           DSTROOT=${destroot}${prefix} RC_ProjectSourceVersion=${version}

post-extract {
    file copy ${worksrcpath}/../ld64-${ld64_version}/src/other/PruneTrie.cpp ${worksrcpath}/misc
    system "touch ${worksrcpath}/../ld64-${ld64_version}/src/abstraction/configure.h"
}

post-patch {
    # We don't want to build cctools ld.  We want to use ld64
    reinplace "/^SUBDIRS_32/s/ld//" ${worksrcpath}/Makefile
    reinplace "/^COMMON_SUBDIRS/s/ ld / /" ${worksrcpath}/Makefile

    # Use our chosen version of llvm
    if {${llvm_version} != ""} {
        reinplace "s:\"llvm-mc\":\"llvm-mc-mp-${llvm_version}\":" ${worksrcpath}/as/driver.c
        reinplace "s:@@LLVM_LIBDIR@@:${prefix}/libexec/llvm-${llvm_version}/lib:" ${worksrcpath}/libstuff/lto.c
    }

    foreach file [glob ${worksrcpath}/{*/,}Makefile] {
        reinplace "s:/usr/local:@PREFIX@:g" ${file}
        reinplace "s:/usr:@PREFIX@:g" ${file}
        reinplace "s:@PREFIX@:${prefix}:g" ${file}
        reinplace "s:${prefix}/efi:${prefix}:g" ${file}
        reinplace "s:/Developer${prefix}:${prefix}:g" ${file}
        reinplace "s:${prefix}/man:${prefix}/share/man:g" ${file}

        # Don't strip installed binaries
        reinplace "s:\\(install .*\\)-s :\\1:g" ${file}

        if {${os.major} < 10} {
            reinplace "s:${prefix}/bin/mig:/usr/bin/mig:g" ${file}
        }
    }
}

use_configure   no
use_parallel_build  yes

# https://trac.macports.org/ticket/43745
configure.cflags-append -std=gnu99 

build.target    all

set cxx_stdlibflags {}
if {[string match *clang* ${configure.cxx}]} {
    set cxx_stdlibflags -stdlib=${configure.cxx_stdlib}
}

configure.cppflags-append -I${worksrcpath}/../ld64-${ld64_version}/src/abstraction -I${worksrcpath}/../ld64-${ld64_version}/src/other -I${worksrcpath}/include

pre-build {
    build.args-append \
        RC_ProjectSourceVersion=${version} \
        USE_DEPENDENCY_FILE=NO \
        BUILD_DYLIBS=NO \
        CC="${configure.cc} ${configure.cflags}" \
        CXX="${configure.cxx} ${configure.cxxflags}" \
        CXXLIB="${cxx_stdlibflags}" \
        TRIE=-DTRIE_SUPPORT \
        RC_ARCHS="[get_canonical_archs]" \
        SDK="${configure.cppflags}"

    if {${llvm_version} != ""} {
        build.args-append \
            LTO=-DLTO_SUPPORT \
            RC_CFLAGS="[get_canonical_archflags] `llvm-config-mp-${llvm_version} --cflags`" \
            LLVM_MC="llvm-mc-mp-${llvm_version}"
    } else {
        build.args-append \
            LTO= \
            RC_CFLAGS="[get_canonical_archflags]"
    }
}

pre-destroot {
    destroot.args-append \
        RC_ProjectSourceVersion=${version} \
        USE_DEPENDENCY_FILE=NO \
        BUILD_DYLIBS=NO \
        CC="${configure.cc} ${configure.cflags}" \
        CXX="${configure.cxx} ${configure.cxxflags}" \
        CXXLIB="${cxx_stdlibflags}" \
        TRIE=-DTRIE_SUPPORT \
        RC_ARCHS="[get_canonical_archs]" \
        SDK="${configure.cppflags}"

    if {${llvm_version} != ""} {
        destroot.args-append \
            LTO=-DLTO_SUPPORT \
            RC_CFLAGS="[get_canonical_archflags] `llvm-config-mp-${llvm_version} --cflags`" \
            LLVM_MC="llvm-mc-mp-${llvm_version}"
    } else {
        destroot.args-append \
            LTO= \
            RC_CFLAGS="[get_canonical_archflags]"
    }
}

platform macosx {
    build.args-append RC_OS="macos"
    destroot.args-append RC_OS="macos"
}

destroot.target install_tools
destroot.args-append DSTROOT=${destroot}
post-destroot {
    file delete -force ${destroot}${prefix}/OpenSourceLicenses
    file delete -force ${destroot}${prefix}/OpenSourceVersions
    file delete -force ${destroot}${prefix}/RelNotes

    if {${os.major} < 10} {
        file delete -force ${destroot}/Developer
    }

    # Provided by port:cctools-headers
    file delete -force ${destroot}${prefix}/include
}

livecheck.type          regex
livecheck.regex         "${name}-(\[\\d.\]+)"
