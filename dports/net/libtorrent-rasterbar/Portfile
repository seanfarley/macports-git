# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           active_variants 1.1
PortGroup           compiler_blacklist_versions 1.0
PortGroup           conflicts_build 1.0
PortGroup           github 1.0

github.setup        arvidn libtorrent 1_0_7 libtorrent-
name                libtorrent-rasterbar
version             [string map {_ .} ${version}]
license             BSD
categories          net
maintainers         devans openmaintainer
platforms           darwin
homepage            http://www.rasterbar.com/products/libtorrent/

description         A C++ bittorrent library (not the same as port libtorrent)

long_description    A C++ bittorrent library designed to run on anything from embedded systems to large scale \
                    seed servers. It comes with python bindings and detailed documentation.

checksums           rmd160  6c731eb3382b4b192fba7ee2fc7fdd1d32f5b23b \
                    sha256  55e2359cc8f7b3ce4c9f2917adc71b7bbe13de21aedc8d2035929728a5233842

depends_build       port:pkgconfig \
                    port:autoconf \
                    port:automake \
                    port:libtool

depends_lib         port:boost \
                    port:libgeoip \
                    path:lib/libssl.dylib:openssl

patchfiles          patch-configure.ac.diff \
                    patch-bindings-python-setup.py.diff

post-patch {
    reinplace "s|__MP_BUILD_ARCH__|${build_arch}|" ${worksrcpath}/bindings/python/setup.py
}

compiler.blacklist  *gcc* {clang < 300}

configure.args      --disable-silent-rules \
                    --enable-python-binding \
                    --enable-geoip \
                    --with-libgeoip \
                    --with-boost-libdir="${prefix}/lib" \
                    --with-boost-python="boost_python-mt"

configure.cmd       ./autotool.sh && ./configure

universal_variant   no

conflicts_build     ${name}

variant python27 description {Build bindings for Python 2.7} {
        require_active_variants boost python27
        depends_lib-append port:python27
        configure.python ${prefix}/bin/python2.7
        configure.env-append \
                "PYTHON_INSTALL_PARAMS=--prefix=${destroot}${prefix}/Library/Frameworks/Python.framework/Versions/2.7" \
                PYTHON_EXTRA_LDFLAGS=' '
}

variant error_logging description {Enable logging of errors to disk} {
        configure.args-append --enable-logging=errors
}

default_variants    +python27
